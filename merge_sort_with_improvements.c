#include <stdio.h>
#include <stdlib.h>
#include "merge.h"

void printArray(int A[], int size)
{
  int i;
  for (i=0; i < size; i++)
    printf("%d ", A[i]);
  printf("\n");
}

void insertion_sort(int *v, int inicio, int fim){
  long int i, j, aux, x;
    
    for(i=inicio; i <= fim; i++){
        x = v[i];
        j = i;
        while(x < v[j-1] && j > 0){
            v[j] = v[j-1];
            j--;
        }
        v[j] = x;
    }
}

void mergeSort(int *arr, int l, int r)
{
  if (l < r)
  {
    int m = l+(r-l)/2;

    if (r - l <= 10)
    {
      printf("\nINSERTION SORT PARA %d ELEMENTOS", r - l);
      insertion_sort(arr, l, r);
    }
    else
    {
      mergeSort(arr, l, m);
      mergeSort(arr, m+1, r);
      merge(arr, l, m, r);
    }
  }
}

int main()
{
  int arr[] = {100, 1058, 349, 3, 6, 2, 9, 1, 8, 3, 5, 23, 52, 2, 4, 85, 105, 10, 11, 23, 3, 999, 9, 15, 1, 8, 3, 5, 23, 52, 2, 4, 85, 105, 10, 11, 23, 3, 999, 9, 15, 1, 8, 3, 5, 23, 52, 2, 4, 85, 105, 10, 11, 23, 3, 999, 9, 15, 1, 8, 3, 5, 23, 52, 2, 4, 85, 105, 10, 11, 23, 3, 999, 9, 15, 1, 8, 3, 5, 23, 52, 2, 4, 85, 105, 10, 11, 23, 3, 999, 9, 15, 1, 8, 3, 5, 23, 52, 2, 4, 85, 105, 10, 11, 23, 3, 999, 9, 15};
  int arr_size = sizeof(arr)/sizeof(arr[0]);

  printf("Given array:\n");
  printArray(arr, arr_size);

  mergeSort(arr, 0, arr_size - 1);

  printf("\n\nSorted array:\n");
  printArray(arr, arr_size);

  return 0;
}
